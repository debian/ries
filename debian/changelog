ries (2018.08.05-1) unstable; urgency=low

  * New upstream version (2018-08-05)
    + Rebase Debian patches

  * Switch to debhelper 12.
    Compatibility level now controlled by a Build-Depends

  * Update debian/copyright
  * debian/rules: Slightly simplify package installation
  * debian/control
    + Update maintainer's email address
    + Declare compliance with policy v4.3.0.
      No change required

 -- Nicolas Braud-Santoni <nicoo@debian.org>  Sat, 23 Feb 2019 19:49:17 +0100

ries (2018.04.11+ds1-1) unstable; urgency=low

  * New version
    + GNUmakefile: Replace wrong LDFLAGS usage with LDLIBS.
      Make compilation with -Wl,--as-needed work.  Closes: #906991
      Thanks to Steve Langasek for reporting the bug  :)

  * Add patches for the manpage
    + Fix spelling mistakes
    + Set the manpages section to 1 (rather than 1L)

  * Make git-buildpackage commit xz-compressed upstream tarballs to pristine-tar
  * debian/copyright
    + Add comment explaining the situation
    + Update the copyright years and ownership
  * Comply with policy version 4.2.1
    + Set Rules-Requires-Root to no

 -- Nicolas Braud-Santoni <nicolas@braud-santoni.eu>  Mon, 27 Aug 2018 22:34:43 +0100

ries (2018.04.11-1) unstable; urgency=medium

  * New upstream version.
  * Declare compliance with policy version 4.1.4.
    No change required

 -- Nicolas Braud-Santoni <nicolas@braud-santoni.eu>  Mon, 30 Apr 2018 13:54:51 +0200

ries (2017.02.12-1) unstable; urgency=medium

  * Initial packaging (Closes: #887561)

 -- Nicolas Braud-Santoni <nicolas@braud-santoni.eu>  Sat, 17 Mar 2018 03:57:49 +0100
